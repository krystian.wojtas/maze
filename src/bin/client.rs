use async_recursion::async_recursion;
use futures_util::SinkExt;
use futures_util::StreamExt;
use serde::{Deserialize, Serialize};
use std::env;
use std::sync::Arc;
use tokio::sync::mpsc;
use tokio::sync::oneshot;
use tokio_tungstenite::{connect_async, tungstenite::protocol::Message};

#[derive(Deserialize, Debug, PartialEq, Copy, Clone)]
struct PointMaybeFinish {
    x: u16,
    y: u16,
    finish: Option<bool>,
}

#[derive(Serialize, Debug, PartialEq, Copy, Clone)]
struct Point {
    x: u16,
    y: u16,
}

impl From<Point> for PointMaybeFinish {
    fn from(point: Point) -> Self {
        Self {
            x: point.x,
            y: point.y,
            finish: None,
        }
    }
}

impl From<PointMaybeFinish> for Point {
    fn from(point_finish: PointMaybeFinish) -> Self {
        Self {
            x: point_finish.x,
            y: point_finish.y,
        }
    }
}

enum Direction {
    Horizontal,
    Vertical,
}

struct Node {
    point: Point,
    previous: Option<Box<Arc<Node>>>,
}

struct Path {
    traversed: Arc<Node>,
    direction: Option<Direction>,
    curves: u32,
}

impl Node {
    fn contains(&self, point: Point) -> bool {
        let mut previous = &self.previous;
        while let Some(node) = previous {
            if node.point == point {
                return true;
            }
            previous = &node.previous;
        }
        false
    }
}

#[derive(Debug)]
struct PointRoutes {
    point: String,
    response: oneshot::Sender<String>,
}

fn figure_out_direction(point1: &Point, point2: &Point) -> Direction {
    let abs_x = (point1.x as i32 - point2.x as i32).abs();
    let abs_y = (point1.y as i32 - point2.y as i32).abs();

    match (abs_x, abs_y) {
        (1, 0) => Direction::Horizontal,
        (0, 1) => Direction::Vertical,
        // Should never happen when assuming that input data always are correct
        // and only gives neighbours
        // TODO proper error propagation
        _ => unimplemented!(),
    }
}

fn figure_out_curves(
    curves_traversed: u32,
    direction: &Direction,
    direction_new: &Direction,
) -> u32 {
    match (direction, direction_new) {
        (Direction::Vertical, Direction::Vertical) => curves_traversed,
        (Direction::Horizontal, Direction::Horizontal) => curves_traversed,
        (Direction::Vertical, Direction::Horizontal) => curves_traversed + 1,
        (Direction::Horizontal, Direction::Vertical) => curves_traversed + 1,
    }
}

#[async_recursion]
async fn walk(path: Path, ws_tx: mpsc::Sender<PointRoutes>, finish_tx: mpsc::Sender<u32>) {
    let current_point = path.traversed.point;
    let routes = get_routes_for_point(&current_point, ws_tx.clone())
        .await
        .unwrap();
    for route in routes {
        // Finish is reached
        if let Some(true) = route.finish {
            finish_tx.send(path.curves).await.unwrap();
            continue;
        }
        // Get rid of finish field
        let route = Point::from(route);
        // Ensure route is not traversed already
        let traversed = Arc::clone(&path.traversed);
        if traversed.contains(route) {
            continue;
        }
        let direction_new = figure_out_direction(&current_point, &route);
        let curves_new = match path.direction {
            // Walk just begin from start point
            None => path.curves,
            Some(ref direction) => figure_out_curves(path.curves, &direction, &direction_new),
        };
        let path_new = Path {
            traversed: Arc::new(Node {
                previous: Some(Box::new(traversed)),
                point: route,
            }),
            direction: Some(direction_new),
            curves: curves_new,
        };
        walk(path_new, ws_tx.clone(), finish_tx.clone()).await;
    }
}

async fn ws(url: String, mut rx: mpsc::Receiver<PointRoutes>) {
    let url = url::Url::parse(&url).unwrap();

    let (mut ws_stream, _) = connect_async(url).await.expect("Failed to connect");

    while let Some(message) = rx.recv().await {
        let msg = Message::Text(message.point);

        ws_stream.send(msg).await.unwrap();

        let buffer = ws_stream.next().await;
        if let Some(Ok(Message::Text(routes))) = buffer {
            message.response.send(routes).unwrap();
        }
    }
}

async fn get_routes_for_point(
    point: &Point,
    ws_tx: mpsc::Sender<PointRoutes>,
) -> Result<Vec<PointMaybeFinish>, String> {

    let point_json = serde_json::to_string(&point).unwrap();

    let (response_tx, response_rx) = oneshot::channel();

    let point_routes = PointRoutes {
        point: point_json,
        response: response_tx,
    };

    ws_tx.send(point_routes).await.unwrap();
    let routes = response_rx.await.unwrap();
    let routes: Vec<PointMaybeFinish> = serde_json::from_str(&routes).unwrap();

    Ok(routes)
}

async fn gather_finished_paths_curves_and_return_minimum(mut rx: mpsc::Receiver<u32>) -> u32 {
    // Should be received at least one finished curves
    let mut curves_minimum = rx.recv().await.unwrap();

    // Keep receiving finished curves until channel is open
    while let Some(curves_finished) = rx.recv().await {
        if curves_finished < curves_minimum {
            curves_minimum = curves_finished;
        }
    }

    curves_minimum
}

#[tokio::main]
async fn main() {
    let connect_addr = env::args()
        .nth(1)
        .unwrap_or_else(|| panic!("Usage: <url to web socket>"));

    // Create a new channel for web sockets
    let (ws_tx, ws_rx) = mpsc::channel(32);

    // Create a new channel for gathering finished routes
    let (finish_tx, finish_rx) = mpsc::channel(32);

    tokio::spawn(ws(connect_addr, ws_rx));

    tokio::spawn(async move {
        let start = Path {
            traversed: Arc::new(Node {
                point: Point { x: 0, y: 1 },
                previous: None,
            }),
            direction: None,
            curves: 0,
        };

        walk(start, ws_tx, finish_tx).await;
    });

    let curves_minimum = tokio::spawn(gather_finished_paths_curves_and_return_minimum(finish_rx))
        .await
        .unwrap();
    println!("{}", curves_minimum);
}
