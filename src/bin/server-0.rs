use futures_util::{SinkExt, StreamExt};
use std::net::SocketAddr;
use tokio::net::{TcpListener, TcpStream};
use tokio_tungstenite::accept_async;
use tokio_tungstenite::tungstenite::protocol::Message;
use tungstenite::Result;

async fn accept_connection(peer: SocketAddr, stream: TcpStream) {
    if let Err(err) = handle_connection(peer, stream).await {
        println!("Error processing connection: {}", err);
    }
}

async fn handle_connection(peer: SocketAddr, stream: TcpStream) -> Result<()> {
    let mut ws_stream = accept_async(stream).await.expect("Failed to accept");

    println!("New WebSocket connection: {}", peer);

    while let Some(msg) = ws_stream.next().await {
        let msg = msg.unwrap();
        if let Message::Text(msg) = msg {
            let response = match msg.trim() {
                r#"{"x":0,"y":1}"# => r#"[{"x":1,"y":1}]"#,
                r#"{"x":1,"y":1}"# => r#"[{"x":2,"y":1}]"#,
                r#"{"x":2,"y":1}"# => r#"[{"x":2,"y":1,"finish":true}]"#,
                _ => "[]",
            };
            ws_stream.send(Message::Text(response.to_string())).await.unwrap();
        }
    }

    Ok(())
}

#[tokio::main]
async fn main() {
    let addr = "127.0.0.1:9002";
    let listener = TcpListener::bind(&addr).await.expect("Can't listen");
    println!("Listening on: {}", addr);

    while let Ok((stream, _)) = listener.accept().await {
        let peer = stream.peer_addr().expect("connected streams should have a peer address");
        println!("Peer address: {}", peer);

        tokio::spawn(accept_connection(peer, stream));
    }
}
